/*
    Copyright (c) 2017 Jean THOMAS.

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the Software
    is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <string.h>

#include <stdint.h>
//#include <CoreFoundation/CoreFoundation.h>
//#include <libkern/OSByteOrder.h>
//#include <IOKit/IOKitLib.h>
//#include <IOKit/IOCFPlugIn.h>
//#include <IOKit/usb/IOUSBLib.h>
//#include <IOKit/usb/USBSpec.h>
#include "main.h"

uint8_t disableplugin_value= 0xba;

int main(int argc, char *argv[]) {
  int i;
  enum TrinityAvailablePower availablePower;
  //IOUSBDeviceInterface300** deviceInterface;
  libusb_device_handle * deviceInterface;
  libusb_device * device;
  if (libusb_init(NULL) != 0)
  {
      return EXIT_FAILURE;
  }
  /* Reading power delivery capacity */
  availablePower = POWER_500MA;
  for (i = 1; i < argc; i++) {
    if (strcmp(argv[i], "--power-500") == 0) {
      printf("Audio device set to 500mA\n");
      availablePower = POWER_500MA;
    } else if (strcmp(argv[i], "--power-1500") == 0) {
      printf("Audio device set to 1500mA\n");
      availablePower = POWER_1500MA;
    } else if (strcmp(argv[i], "--power-3000") == 0) {
      printf("Audio device set to 3000mA\n");
      availablePower = POWER_3000MA;
    } else if (strcmp(argv[i], "--power-4000") == 0) {
      printf("Audio device set to 4000mA\n");
      availablePower = POWER_4000MA;
    }
  }

  if (availablePower == POWER_NULL) {
    printf("Available power settings :\n");
    printf("\t--power-500\t500mA\n");
    printf("\t--power-1500\t1500mA\n");
    printf("\t--power-3000\t3000mA\n");
    printf("\t--power-4000\t4000mA\n");
    
    return EXIT_SUCCESS;
  }

  /* Getting USB device interface */
  //deviceInterface = usbDeviceInterfaceFromVIDPID(0x05AC,0x1101);
  deviceInterface = libusb_open_device_with_vid_pid(NULL, 0x05AC, 0x1101);
  if (deviceInterface == NULL) {
      printf("Error Opening Device");
    return EXIT_FAILURE;
  }

  /* Opening USB device interface */

  device = libusb_get_device(deviceInterface);
  printf("Found Device Speed: %d\n", libusb_get_device_speed (device));


  if (disablePlugin(deviceInterface) != 0) {
    printf("Error while disabling plugin.\n");
    return EXIT_FAILURE;
  }
  if (downloadEQ(deviceInterface, availablePower) != 0) {
    printf("Error while downloading EQ to Trinity audio device.\n");
    return EXIT_FAILURE;
  }
  if (downloadPlugin(deviceInterface) != 0) {
    printf("Error while downloading plugin to Trinity audio device.\n");
    return EXIT_FAILURE;
  }
  if (enablePlugin(deviceInterface) != 0) {
    printf("Error while enabling plugin.\n");
    return EXIT_FAILURE;
  }

  /* Closing the USB device */
  libusb_close(deviceInterface);
  libusb_exit(NULL);
  return EXIT_SUCCESS;
}


int xdfpSetMem(libusb_device_handle * deviceInterface, uint8_t *buf, uint16_t length, uint16_t xdfpAddr) {
  //Build a packet and submit a control request to the device

    if (libusb_control_transfer (deviceInterface, (LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_OUT), kMicronasSetMemReq, 0, xdfpAddr, buf, length, 2000) < 0)
    {
        printf("Error in Control signal");
        return -1;
    }
  return 0;
}

int xdfpWrite(libusb_device_handle * deviceInterface, uint16_t xdfpAddr, int32_t value) {
  static uint8_t xdfpData[5];

    if (value < 0) value += 0x40000;
    xdfpData[0] = (value >> 10) & 0xff;
    xdfpData[1] = (value >> 2) & 0xff;
    xdfpData[2] = value & 0x03;
    xdfpData[3] = (xdfpAddr >> 8) & 0x03;
    xdfpData[4] = xdfpAddr & 0xff;

    return xdfpSetMem(deviceInterface, xdfpData, 5, V8_WRITE_START_ADDR);
}

int downloadEQ(libusb_device_handle * deviceInterface, enum TrinityAvailablePower availablePower) {
  uint16_t xdfpAddr;
  uint32_t  eqIndex;
  int ret;
  static int32_t *eqSettings;

  switch (availablePower) {
  case POWER_4000MA:
    eqSettings = power4AEQSettings;
    break;
  case POWER_3000MA:
    eqSettings = power3AEQSettings;
    break;
  case POWER_1500MA:
    eqSettings = power1500mAEQSettings;
    break;
  default:
  case POWER_500MA:
    eqSettings = power500mAEQSettings;
    break;
  }
  
  for (eqIndex = 0, xdfpAddr = XDFP_STARTING_EQ_ADDR; eqIndex < EQ_TABLE_SIZE; eqIndex++, xdfpAddr++) {
    ret = xdfpWrite(deviceInterface, xdfpAddr, eqSettings[eqIndex]);
    if (ret != 0) {
      return ret;
    }
    
    nanosleep((const struct timespec[]){{0, 3000000L}}, NULL);
  }

  return ret;
}

int disablePlugin(libusb_device_handle * deviceInterface) {
  return xdfpSetMem(deviceInterface, &disableplugin_value, 1, V8_PLUGIN_START_ADDR);
}
int enablePlugin(libusb_device_handle * deviceInterface) {
  return xdfpSetMem(deviceInterface, pluginBinary, 1, V8_PLUGIN_START_ADDR);
}

int downloadPlugin(libusb_device_handle * deviceInterface) {
  return xdfpSetMem(deviceInterface, &pluginBinary[1], sizeof(pluginBinary), V8_PLUGIN_START_ADDR+1);
}
